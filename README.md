**Weather Prediction During World War-II**

This Project Predicts the Weather Conditions during World War II using Machine Learning in Python.

It is based on _Linear Regression Model_.

**Source of Dataset:**
- Dataset is imported from "_https://y.foxmula.com/projects_"

**Developed By:**
- Manish Kumar (officialuse465@gmail.com)